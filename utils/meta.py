from .models import Command


class BotMeta(type):
    def __new__(cls, *args, **kwargs):
        self = super().__new__(cls , *args, **kwargs)

        commands = []

        for _, val in self.__dict__.items():
            if isinstance(val, Command):
                commands.append(val)

        self.__commands_list__ = commands
        return self
