"""
Models usados no bot e na api.
"""
from aiogram.types import Message
from inspect  import signature

from typing import (Union,
					Callable,
					Optional)


class Command:
	def __init__(self, callback, name, **kw):
		self._callback = callback
		
		if not isinstance(name, str):
			raise TypeError("Nome de um comando deve de ser uma `str`.")
		elif not isinstance(kw.get("aliases", []), list):
			raise TypeError("Aliases têm de ser um tipo list.")
			
		self.name = name
		self.description = kw.get("description", " ")
		self.hidden = kw.get("hidden", False)
		self.aliases = kw.get("aliases", [])
		self.params = len(signature(self._callback).parameters)

		self.cooldown: Optional[float] = kw.get("cooldown", None)

	async def __call__(self, *args, **kw):
		await self._callback(*args, **kw)


class Group(Command):
	def __init__(self, callback, name, **kw):
		super().__init__(callback, name, **kw)
		self.__sub_commands__: dict = {}

	@property
	def commands(self) -> set:
		return set(self.__sub_commands__.values())

	def command(self, name: Union[str, list, tuple], cls: object=None, **kw) -> Callable:
		if cls is None:
			cls = Command

		def decorator(func):
			command = cls(func, name, **kw)

			if command.name in self.__sub_commands__.keys():
				raise TypeError("Parece que o comando já esta registrado.")

			self.__sub_commands__[command.name] = command
			for aliase in command.aliases:
				if aliase in self.__sub_commands__.keys():
					raise TypeError("Aliases já registradas.")
				self.__sub_commands__[aliase] = command

			return command

		return decorator

	def get_subcommand(self, name: str) -> Optional[Command]:
		return self.__sub_commands__.get(name)

	async def process_commands(self, message: Message) -> None:
		"""Este method ira processar os argumentos do comamdo,
		e ira executar o comando ou subcommand."""

		args = message.get_args().split()
		try:
			subcommand_name = args[0]
		except IndexError:
			await self(message, *args[1:self.params])
			return

		if (subcommand := self.get_subcommand(subcommand_name)):
			print(subcommand.params)
			await subcommand(message, *args[1:subcommand.params])
			return
		
		await self(message, *args[1:self.params])