from typing import Dict, List, Union

def gerate_table(rows: Dict[str, List[Union[str, int]]], *, title: str=None) -> str:
    """Method usado para gerar ascii table
    
    Parametros
    ----------
    rows: :class:`Dict[str, List[Union[str, int]]]`
        Nome das rows da table e values

    Returns
    -------
    :class:`str`
        ascii table
    """
    final_val: str = "┌" + "┬".join(["─"*(len(x)+2) for x in rows.keys()]) + "┐\n"
    
    final_val += "│ " + " │ ".join([f'{x}' for x in rows.keys()]) + " |\n"

    #for val in rows.values():
         

    final_val += "└" + "┴".join(["─"*(len(x)+2) for x in rows.keys()]) + "┘\n"

    return final_val
    #print(final_val)
    """ for key, val in rows.items():
        final_val += f"│ {key} │"

    final_val = "┌" + "┬".join(["─"*len(x) for x in rows]) + "┐\n"

    for value in values:
        final_val += "".join([x for x in value]) """
