import logging
import time

from aiogram import Bot, Dispatcher, executor
from api import CoinGecko
from utils import Command, Group, BotMeta

from asyncio import BaseEventLoop, AbstractEventLoop
from typing import Union, Callable, Dict


class CryptoBot(Bot):
    def __init__(
        self,
        token: str,
        *,
        loop: Union[BaseEventLoop, AbstractEventLoop] = None,
        timeout: int = None,
        debug: bool,
        **kw
    ):
        super().__init__(token, loop=loop, timeout=timeout, **kw)
        self._debug = debug

        self.uptime = time.time()
        self.coin_gecko = CoinGecko(loop=loop)
        self.dp = Dispatcher(self)

        self.setup_logging()
        self._logger = logging.getLogger(__name__)

        self.__commands_list__: Dict[str, Callable] = {}

    def setup_logging(self):
        logging.basicConfig(
            format="[%(asctime)s] [%(levelname)s] [%(name)s] - %(message)s",
            level=logging.WARNING if not self._debug else logging.DEBUG,
        )

    def add_command(self, command: Command) -> None:
        if not isinstance(command, Command):
            raise TypeError("Comando não é valido.")

        if command.name in self.__commands_list__.keys():
            raise TypeError("Parece que já existe um comando com o mesmo nome.")

        self.__commands_list__[command.name] = command
        self.dp.register_message_handler(
                                command.process_commands,
                                commands=command.aliases,  # registar message handler
                            )

        self._logger.info(f"Command {command.aliases} foi registrado com sucesso com callback em {getattr(command, 'process_commands', command._callback)}")

    def command(self, name: str, cls: object=None, **kw) -> Callable:
        if cls is None:
            cls = Command

        def decorator(func):
            command = cls(func, name, **kw)

            self.add_command(command)
            return command

        return decorator

    def group(self, **attrs) -> Callable:
        attrs.setdefault("cls", Group)
        return self.command(**attrs)

    def get_command(self, name: str) -> Command:
        name = name.lstrip("/")  # remove prefix from command name.

        if " " not in name:
            return self.__commands_list__.get(name)

        return None