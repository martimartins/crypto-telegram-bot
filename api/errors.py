class CoinGeckoApiError(Exception):
    """Base error class para outros errors."""

class NotFound(CoinGeckoApiError):
    """Quando api call retorna status code 404"""

class TimeoutErr(CoinGeckoApiError):
    """Quando api retorna status code 403"""