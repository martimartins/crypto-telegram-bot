import aiohttp
import logging

from asyncio import BaseEventLoop, AbstractEventLoop, get_event_loop, sleep
from sys import version_info
from typing import Any, AsyncGenerator, Union, Optional

from . import __version__, errors


__all__ = ("Moeda", "Route", "CoinGecko")


_logger = logging.getLogger(__name__)


class Moeda:
    """Representação de uma crypto moeda

    Attributes
    ----------
    name: :class:`str`
        Nome da crypto moeda.
    symbol: :class:`str`
        Symbolu da crypto moeda.
    hashing_algorithm: :class:`str`
        Hash algoritmo usado na crypto moeda.
    description: :class:`str`
        Descricão da crypto moeda.
    github: :class:`str`
        Repo do github da crypto moeda.
    avatar: :class:`str`
        Avatar url da moeda (small)
    total_supply: :class:`int`
        Total Supply da crypto moeda.
    max_supply: :class:`int`
        Max supply da crypto moeda.
    circulating_supply: :class:`int`
        Supply em cilculação da crypto moeda.
    trust_score: :class:`str`:
        Trust score da crypto moeda.
    """

    __slots__ = (
        "_market_data",
        "name",
        "symbol",
        "hashing_algorithm",
        "description",
        "github",
        "avatar",
        "total_supply",
        "max_supply",
        "circulating_supply",
        "trust_score",
        "market_cap_rank",
    )

    def __init__(self, data: dict) -> None:
        self._market_data = data.get("market_data")
        self.name = data.get("name")
        self.symbol = data.get("symbol")
        self.hashing_algorithm = data.get("hashing_algorithm")
        self.description = data.get("description", {}).get("pt")
        self.github = data.get("repos_url", {}).get("github")
        self.avatar = data.get("image", {}).get("small") or data.get("small")
        self.total_supply = data.get("total_supply")
        self.max_supply = data.get("max_supply")
        self.circulating_supply = data.get("circulating_supply")
        self.trust_score = data.get("trust_score")
        self.market_cap_rank = data.get("market_cap_rank")

    def get_price(self, currency: str = "eur") -> Optional[Union[int, float]]:
        return self._market_data.get("current_price", {}).get(currency)

    def get_price_change_percentage(self, intime: str) -> str:
        if intime.lower() == "1h":
            return f"{self._market_data.get(f'price_change_percentage_1h_in_currency').get('eur'):.1f}%"
        return f"{self._market_data.get(f'price_change_percentage_{intime}'):.1f}%"

    def get_price_change(self, currency: str="eur") -> float:
        return self._market_data.get("price_change_24h_in_currency", {}).get(currency)


class Route:
    _API_BASE = "https://api.coingecko.com/api/v3/"

    def __init__(self, path: str, method: str) -> None:
        self.url = self._API_BASE + path
        self.method = method


class CoinGecko:
    def __init__(
        self, *, session=None, loop: Union[BaseEventLoop, AbstractEventLoop]
    ) -> None:
        self.__session = session or self._session
        self.loop = loop or get_event_loop()

        self.user_agent = "CoinGeckoAPI {0} Python {1[0]}.{1[1]} aiohttp {2}".format(
            __version__, version_info, aiohttp.__version__
        )

    @property
    def _session(self):
        return aiohttp.ClientSession()

    async def __request(self, route: Route, **kw) -> Optional[aiohttp.ClientResponse]:
        """|coro|

        Method para fazer request."""
        assert isinstance(route, Route), "Route não é valido."

        kw["headers"] = {
            "User-Agent": self.user_agent,
            "X-Ratelimit-Precision": "millisecond",
        }

        for _ in range(5):  # 5 tentativas para fazer o request.
            async with self.__session.request(route.method, route.url, **kw) as r:
                _logger.debug("%s %s returnou %s.", route.method, route.url, r.status)

                data = await r.json()

                if r.ok:
                    return r

                if r.status == 404:
                    raise errors.NotFound()
                elif r.status == 403:
                    _logger.warning("Rate limit from %s", route.url)
                    await sleep(data.get("retry_after", 60))
                    continue
                elif r.status == 503:
                    raise errors.CoinGeckoApiError()

        raise errors.TimeoutErr()

    async def ping_api(self) -> int:
        response = await self.__request(Route(path="ping"))

        return response.status  # 200: OK

    async def fetch_coin(self, coin_id: str) -> Moeda:
        response = await self.__request(
            Route(path="coins/{0}".format(coin_id), method="get")
        )

        if not response:
            return

        data = await response.json()
        return Moeda(data)

    async def fetch_coin_price(self, coin_id: str, currency: str) -> int:
        response = await self.__request(
            Route(path="coins/{0}".format(coin_id), method="get")
        )

        data = await response.json()
        return data.get(coin_id).get(currency)

    async def fetch_treading(self, *, raw: bool = False) -> AsyncGenerator[Moeda, Any]:
        response = await self.__request(Route(path="search/trending", method="get"))

        data = await response.json()
        for coin_data in data.get("coins", []):
            
            if raw:
                yield coin_data.get("item")
                continue
            
            yield Moeda(coin_data.get("item"))
