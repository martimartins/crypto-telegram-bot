from bot import CryptoBot
from configs import TOKEN

from aiogram import types, executor
from tabulate import tabulate

client = CryptoBot(TOKEN, debug=True)


@client.dp.message_handler(commands=["teste"])
async def teste_command(message: types.Message, arg1: str):
    await message.reply("Hi there!")


@client.group(
    name="coin",
    aliases=["coins", "token"],
    description="Este comando ira dar informações sobre uma token/coin",
)
async def coin_command(ctx: types.Message):
    await ctx.answer("MAIN COMMAND!")


@coin_command.command(
    name="price",
    aliases=["preço", "preco"],
    description="Este comando ira dar informações sobre o preço da token/coin",
)
async def coin_price_subcommand(ctx: types.Message, coin: str):
    print(coin.lower().replace(" ", "-"))
    coin = await client.coin_gecko.fetch_coin(coin.lower().replace(" ", "-"))

    await ctx.answer(
        """```diff\n\t\t\t\t\t{0.name} ({0.symbol})\n\nPreço atual: {1:,}€ ({2:.1f}€ in 24h)\n\n\n{3}```""".format(
            coin,
            coin.get_price(),
            coin.get_price_change(),
            tabulate(
                [
                    [
                        coin.get_price_change_percentage("1h"),
                        coin.get_price_change_percentage("24h"),
                        coin.get_price_change_percentage("7d"),
                        coin.get_price_change_percentage("14d"),
                        coin.get_price_change_percentage("30d"),
                        coin.get_price_change_percentage("1y"),
                    ]
                ],
                headers=["1h", "24h", "7d", "14d", "30d", "1y"],
                tablefmt="fancy_grid",
            ),
        ),
        parse_mode="markdown",
    )


@coin_command.command(
    name="description",
    aliases=["descrição", "descricao"],
    description="Este comando ira dar descrição sobre a token/coin",
)
async def description_subcommand(ctx: types.Message, coin: str):
    coin = await client.coin_gecko.fetch_coin(coin)

    await ctx.answer(coin.desc)


@coin_command.command(
    name="top",
    aliases=["trending", "leaderboard"],
    description="Este comando ira dar list de coins mais populares primeiro",
)
async def top_subcommand(ctx: types.Message):
    await ctx.answer(
        "**Trending Coins (in 24h)**\n--------------------------------------\n"
        + "\n\n".join(
            [
                f"#{c.get('score')+1} {c.get('name')} ({c.get('symbol')})\t | `market_cap_rank: #{c.get('market_cap_rank')}`, `price {c.get('price_btc'):.1f}₿ (BTC)`"
                async for c in client.coin_gecko.fetch_treading(raw=True)
            ]
        ),
        parse_mode="markdown"
    )

if __name__ == "__main__":
    executor.start_polling(client.dp, skip_updates=True)
